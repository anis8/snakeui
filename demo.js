import {Snake} from "./snake/Snake.js";

let S = Snake;
S.ready(function () {
    //language=HTML
    let template = `
        <div>
            <div>Titre</div>
            <div>Nom</div>
        </div>
    `;
    let animation = 0;
    let scale = 0;
    S.el("anim5").draggable(true);
    let copy = S.el("anim6").copy();
    S.el("anim7").append(copy);
    copy.setHtml("Hello copy");
    setInterval(function () {
        switch (animation) {
            case 0:
                animation++;
                S.el("anim1").moveRight(0, 1000);
                S.el("anim2").moveBottom(0, 1000);
                break;
            case 1:
                S.el("anim1").moveBottom(0, 1000);
                S.el("anim2").moveRight(0, 1000);
                animation++;
                break;
            case 2:
                S.el("anim1").moveLeft(0, 1000);
                S.el("anim2").moveTop(0, 1000);
                animation++;
                break;
            case 3:
                S.el("anim1").moveTop(0, 1000);
                S.el("anim2").moveLeft(0, 1000);
                animation = 0;
                break;
        }

        switch (scale) {
            case 0:
                scale++;
                S.el("anim3").scale(1, 2, 650);
                S.el("anim4").fade("in", 650);
                S.el("anim8").moveTop(0, 500);
                S.el("anim6").moveLeft(0, 500);
                copy.moveRight(0, 500);
                break;

            case 1:
                scale = 0;
                S.el("anim3").scale(2, 1, 650);
                S.el("anim4").fade("out", 650);
                S.el("anim8").moveBottom(0, 500);
                S.el("anim6").moveRight(0, 500);
                //copy.moveLeft(0, 500);
                break;
        }
    }, 650);
    copy.setHtml("Hello copy");


    let onclick = false;
    S.el("anim8").onclick(function () {
        if (onclick) {
            onclick = false;
            S.el("anim8").setStyle("border:6px solid white;")
        } else {
            S.el("anim8").setStyle("border:3px solid white;")
            onclick = true;
        }
    });

    let appendI = 0;
    setInterval(function () {
        appendI++;
        let append = S.el("anim9").append('<div class="red"></div>');
        append.setHtml(appendI);
        append.moveBottom(0, 1000);

        setTimeout(function () {
            append.remove();
        }, 1500);
    }, 2000);

    let save = true;
    let x = S.el("anim10");
    setInterval(function () {
        if (save) {
            x.save();
            x.setStyle("height:200px;");
            x.setHtml("Saved");
            save = false;
        } else {
            x.restore();
            save = true;
        }
    }, 1500);

    let ed = S.ed("anim11");

    ed.editable.setStyle("height:150px;");
    ed.setPlaceholder("Veuillez écrire quelque chose");
    ed.editor();
    ed.urlify();
});
