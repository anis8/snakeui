<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type='module' src='demo.js'></script>
</head>
<body>

<style>
    body {
        position: relative;
        margin: 0;
        width: calc(90% + 20px);
        left: 5%;
    }

    .block {
        position: relative;
        width: calc(50% - 20px);
        border-right: 10px solid white;
        border-bottom: 10px solid white;
        float: left;
    }

    .blue {
        position: relative;
        height: 400px;
        width: 100%;
        border-radius: 10px;
        background-image: linear-gradient(to bottom, #066aff, #1653d9, #163db4, #0f2991, #03156f);
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .title {
        height: 100px;
        width: 100%;
        font-size: 200%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .yellow {
        position: absolute;
        height: 250px;
        width: 250px;
        border-radius: 10px;
        background-image: linear-gradient(to bottom, #eafa70, #dae059, #cac742, #baaf2a, #a9970e);
        box-shadow: 0 0 30px -10px #00000073;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .white {
        position: absolute;
        width: 400px;
        border-radius: 10px;
        background: white;
        box-shadow: 0 0 30px -10px #00000073;
    }

    .red {
        position: absolute;
        height: 100px;
        width: 100px;
        border-radius: 30px;
        box-shadow: 0 0 30px -10px #00000073;
        background-image: linear-gradient(to bottom, #e84949, #d2403f, #bd3636, #a82d2d, #942424);
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 200%;
    }

    ul {
        position: relative;
        clear: both;
        width: 100%;
        height: 70px;
        background: red;
        margin: 0px 0px 55px 0px;
        padding: 0;
    }

    button {
        position: relative;
        float: left;
    }
</style>
<div class="title">
    SnakeUI
</div>
<div class="block">
    <div class="title">
        Animations
    </div>
    <div class="blue">
        <div id="anim1" class="yellow">
            <div id="anim2" class="red"></div>
        </div>
    </div>
</div>
<div class="block">
    <div class="title">
        Scale
    </div>
    <div class="blue">
        <div id="anim3" class="red"></div>
    </div>
</div>

<div class="block">
    <div class="title">
        Fade
    </div>
    <div class="blue">
        <div id="anim4" class="yellow"></div>
    </div>
</div>

<div class="block">
    <div class="title">
        Draggable
    </div>
    <div class="blue">
        <div id="anim5" class="yellow"></div>
    </div>
</div>

<div class="block">
    <div class="title">
        Copy element
    </div>
    <div id="anim7" class="blue">
        <div id="anim6" class="yellow">
            Hello original
        </div>
    </div>
</div>

<div class="block">
    <div class="title">
        Virtual element onclick
    </div>
    <div class="blue">
        <div id="anim8" class="yellow"></div>
    </div>
</div>

<div class="block">
    <div class="title">
        Create & remove virtual element
    </div>
    <div class="blue">
        <div id="anim9" class="yellow"></div>
    </div>
</div>


<div class="block">
    <div class="title">
        Save & Restore element
    </div>
    <div class="blue">
        <div id="anim10" class="red">Restore</div>
    </div>
</div>

<div class="block">
    <div class="title">
        Text editor
    </div>
    <div class="blue">
        <div style="width:100%;" class="white">
            <div id="anim11"></div>
        </div>
    </div>
</div>

</body>
</html>