import {Element} from "./elements/Element.js";
import {Editable} from "./elements/types/editable/Editable.js";
import {Ajax} from "./network/Ajax.js";

export class Snake {
    static el(elementId) {
        let element = document.getElementById(elementId);
        return new Element(element);
    }

    static ed(elementId) {
        let element = document.getElementById(elementId);
        return new Editable(element);
    }

    static ajax(host, port, request) {
        Ajax.request(host, port, request)
    }

    static ready(callback) {
        if (document.readyState !== 'loading') callback();
        else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
        else document.attachEvent('onreadystatechange', function () {
                if (document.readyState === 'complete') callback();
            });
    }

}