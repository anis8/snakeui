export class Ajax {
    static request(host, port, request) {
        let url = "http" + (typeof request.ssl !== "undefined") ? "s" : "" + "://" + host + ":" + port + "/" + request.pathname;
        let xhr = new XMLHttpRequest();
        xhr.open(request.type, url, true);
        if (typeof request.crossOrigin !== "undefined") {
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }
        xhr.onloadend = function (e) {
            if (xhr.status === 200) {
                if (typeof request.dataType !== "undefined") {
                    if (request.dataType === "json") {
                        request.success(JSON.parse(e.currentTarget.response));
                    }
                } else {
                    request.success(e);
                }
            } else {
                request.error(e);
            }
        };
        xhr.onerror = function (e) {
            request.error(e);
        };


        let dataSize = Object.keys(request.data).length;
        if (typeof request.data !== "undefined" && dataSize > 0) {
            let data = "";
            let counter = 1;
            Object.keys(request.data).forEach(function (k) {
                data += k + "=" + request.data[k] + (counter >= dataSize ? "" : "&");
                counter++;
            });
            xhr.send(data);
        } else {
            xhr.send();
        }
    }
}