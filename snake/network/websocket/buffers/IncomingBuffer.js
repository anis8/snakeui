export class IncomingBuffer {

    constructor(buffer) {
        this.buffer = null;
        this.bufferSlot = 0;
        this.view = null;


        this.buffer = buffer;
        this.view = new DataView(buffer);
        let size = this.view.getInt32(this.bufferSlot) + 4;
        this.bufferSlot = this.bufferSlot + 4;
        if (size > this.buffer.length) {
            console.log("SNK WS: incoming packet: Incomplete.");
        }
    }

    clear() {
        this.buffer = null;
        this.bufferSlot = 0;
        this.view = null;
    }

    readInt() {
        let result = this.view.getInt32(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 4;
        return result;
    }

    readChar() {
        let result = this.view.getInt16(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 2;
        return String.fromCharCode(result);
    }

    readBoolean() {
        let result = this.view.getInt8(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 1;
        return !!result;
    }

    readShort() {
        let result = this.view.getInt16(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 2;
        return result;
    }

    readByte() {
        let result = this.view.getInt8(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 1;
        return result;
    }

    readFloat() {
        let result = this.view.getFloat32(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 4;
        return result;
    }

    readLong() {
        let result = this.view.getBigInt64(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 8;
        return BigInt(result);
    }

    readDouble() {
        let result = this.view.getFloat64(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 8;
        return result;
    }

    readString() {
        let size = (this.view.getInt16(this.bufferSlot));
        this.bufferSlot = this.bufferSlot + 2;
        let arrayString = [];
        for (let i = 0; i < size; i++) {
            arrayString.push(this.view.getUint8(this.bufferSlot));
            this.bufferSlot++;
        }

        let out, i, len, c, char2, char3;
        out = "";
        len = arrayString.length;
        i = 0;
        while (i < len) {
            c = arrayString[i++];
            switch (c >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    out += String.fromCharCode(c);
                    break;
                case 12:
                case 13:
                    char2 = arrayString[i++];
                    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                    break;
                case 14:
                    char2 = arrayString[i++];
                    char3 = arrayString[i++];
                    out += String.fromCharCode(((c & 0x0F) << 12) |
                        ((char2 & 0x3F) << 6) |
                        ((char3 & 0x3F) << 0));
                    break;
            }
        }
        return out;
    }
}