export class OutgoingBuffer {

    constructor() {
        this.array = [];
        this.buffer = null;
        this.bufferSize = 0;
        this.bufferSlot = 0;
    }


    clear() {
        this.array = [];
        this.buffer = null;
        this.bufferSize = 0;
        this.bufferSlot = 0;
    }

    writeInt(value) {
        this.array.push(["int", value]);
        this.bufferSize = this.bufferSize + 4;
    }

    writeBoolean(value) {
        this.array.push(["boolean", value]);
        this.bufferSize = this.bufferSize + 1;
    }

    writeByte(value) {
        this.array.push(["byte", value]);
        this.bufferSize = this.bufferSize + 1;
    }

    writeShort(value) {
        this.array.push(["short", value]);
        this.bufferSize = this.bufferSize + 2;
    }

    writeFloat(value) {
        this.array.push(["float", value]);
        this.bufferSize = this.bufferSize + 4;
    }

    writeLong(value) {
        this.array.push(["long", value]);
        this.bufferSize = this.bufferSize + 8;
    }

    writeDouble(value) {
        this.array.push(["double", value]);
        this.bufferSize = this.bufferSize + 8;
    }

    writeChar(value) {
        this.array.push(["char", value]);
        this.bufferSize = this.bufferSize + 2;
    }

    writeString(value) {
        this.array.push(["string", value]);
        this.bufferSize = (this.bufferSize + (value.length * 2)) + 2;
    }

    commit() {
        this.buffer = new ArrayBuffer(this.bufferSize + 4);
        let view = new DataView(this.buffer);
        view.setInt32(this.bufferSlot, this.bufferSize);
        this.bufferSlot = this.bufferSlot + 4;
        for (let i = 0; i < this.array.length; i++) {
            switch (this.array[i][0]) {
                case "int":
                    view.setInt32(this.bufferSlot, this.array[i][1]);
                    this.bufferSlot = this.bufferSlot + 4;
                    break;
                case "char":
                    view.setInt16(this.bufferSlot, this.array[i][1].charCodeAt(0));
                    this.bufferSlot = this.bufferSlot + 2;
                    break;
                case "boolean":
                    view.setInt8(this.bufferSlot, this.array[i][1] ? 1 : 0);
                    this.bufferSlot = this.bufferSlot + 1;
                    break;
                case "byte":
                    view.setInt8(this.bufferSlot, this.array[i][1]);
                    this.bufferSlot = this.bufferSlot + 1;
                    break;
                case "short":
                    view.setInt16(this.bufferSlot, this.array[i][1]);
                    this.bufferSlot = this.bufferSlot + 2;
                    break;
                case "float":
                    view.setFloat32(this.bufferSlot, this.array[i][1]);
                    this.bufferSlot = this.bufferSlot + 4;
                    break;
                case "long":
                    view.setBigInt64(this.bufferSlot, BigInt(this.array[i][1]));
                    this.bufferSlot = this.bufferSlot + 8;
                    break;
                case "double":
                    view.setFloat64(this.bufferSlot, this.array[i][1]);
                    this.bufferSlot = this.bufferSlot + 8;
                    break;
                case "string":
                    let str = this.array[i][1];
                    let utf8 = [];
                    for (let i = 0; i < str.length; i++) {
                        let charcode = str.charCodeAt(i);
                        if (charcode < 0x80) utf8.push(charcode);
                        else if (charcode < 0x800) {
                            utf8.push(0xc0 | (charcode >> 6), 0x80 | (charcode & 0x3f));
                        } else if (charcode < 0xd800 || charcode >= 0xe000) {
                            utf8.push(0xe0 | (charcode >> 12),
                                0x80 | ((charcode >> 6) & 0x3f),
                                0x80 | (charcode & 0x3f));
                        } else {
                            i++;
                            charcode = 0x10000 + (((charcode & 0x3ff) << 10)
                                | (str.charCodeAt(i) & 0x3ff));
                            utf8.push(0xf0 | (charcode >> 18),
                                0x80 | ((charcode >> 12) & 0x3f),
                                0x80 | ((charcode >> 6) & 0x3f),
                                0x80 | (charcode & 0x3f));
                        }
                    }
                    view.setInt16(this.bufferSlot, utf8.length);
                    this.bufferSlot = this.bufferSlot + 2;
                    for (let x = 0; x < utf8.length; x++) {
                        view.setUint8(this.bufferSlot, utf8[x]);
                        this.bufferSlot = this.bufferSlot + 1;
                    }
                    break;
            }
        }
    }
}