export class BytesSocket {
    initChannel(host, port, handler, error, close) {
        this.host = host;
        this.port = port;
        this.connection = new WebSocket("ws://" + this.host + ":" + this.port + "/");
        this.connection.binaryType = "arraybuffer";

        this.connection.onmessage = function (e) {
            new handler(e);
        };

        this.connection.onerror = function (e) {
            new error(e);
        };

        this.connection.onclose = function (e) {
            new close(e);
        }
    };

    channelSend(arrayMessage) {
        console.log(arrayMessage.array);
        this.connection.send(arrayMessage.buffer);
    }

    onChannelReady() {
        return new Promise((resolve) => {
            this.connection.onopen = function () {
                resolve();
            }
        })
    }


}