import {Snake} from "../Snake.js";

export class Element {
    constructor(element) {
        this.element = element;
        this.listeners = [];
    };

    frame(startValue, goalValue, speed, func, round) {
        let start = performance.now();
        requestAnimationFrame(function animate(time) {
            let timeFraction = (time - start) / speed;
            if (timeFraction > 1) timeFraction = 1;
            let progress = startValue + (timeFraction * (goalValue - startValue));
            if (typeof round === "undefined") {
                progress = Math.round(progress);
            }
            func(progress);
            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            }
        });
    };

    animateLeft(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().left, this.getParentOffsets().left + value, speed, function (val) {
            self.element.style.left = val + "px";
        });
        this.element.style.right = null;
    };

    animateTop(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().top, this.getParentOffsets().top + value, speed, function (val) {
            self.element.style.top = val + "px";
        });
        this.element.style.bottom = null;
    };

    animateRight(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().right, this.getParentOffsets().right + value, speed, function (val) {
            self.element.style.right = val + "px";
        });
        this.element.style.left = null;
    };

    animateBottom(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().bottom, this.getParentOffsets().bottom + value, speed, function (val) {
            self.element.style.bottom = val + "px";
        });
        this.element.style.top = null;
    };

    animateWidth(value, speed) {
        let self = this;
        this.frame(this.getWidth(), value, speed, function (val) {
            self.element.style.width = val + "px";
        });
    };

    animateHeight(value, speed) {
        let self = this;
        this.frame(this.getHeight(), value, speed, function (val) {
            self.element.style.height = val + "px";
        });
    };

    moveLeft(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().left, value, speed, function (val) {
            self.element.style.left = val + "px";
        });
        this.element.style.right = null;
    };

    moveTop(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().top, value, speed, function (val) {
            self.element.style.top = val + "px";
        });
        this.element.style.bottom = null;
    };

    moveRight(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().right, value, speed, function (val) {
            self.element.style.right = val + "px";
        });
        this.element.style.left = null;
    };

    moveBottom(value, speed) {
        let self = this;
        this.frame(150, value, speed, function (val) {
            self.element.style.bottom = val + "px";
        });
        this.element.style.top = null;
    };

    scale(start, value, speed) {
        let self = this;
        self.element.style.transform = "scale(" + start + ")";
        this.frame(start, value, speed, function (val) {
            self.element.style.transform = "scale(" + val + ")";
        }, false);
    };

    fade(value, speed) {
        let self = this;
        self.element.style.opacity = (value === "in" ? "0" : "1");
        this.frame((value === "in" ? 0 : 1), (value === "in" ? 1 : 0), speed, function (val) {
            self.element.style.opacity = val;
        }, false);

    };

    draggable(inside, element) {
        element = (typeof element !== "undefined" ? element : this);
        let self = this;
        let initX, initY, mousePressX, mousePressY;
        let down = false;
        let parent = (typeof inside !== "undefined") ? element.getParent() : null;

        let touchDown = function (e) {
            e.preventDefault();
            initX = element.element.offsetLeft;
            initY = element.element.offsetTop;
            let touch = e.touches;
            mousePressX = touch[0].pageX;
            mousePressY = touch[0].pageY;
            self.element.addEventListener('touchmove', move, false);
            window.addEventListener('touchend', touchUp, false);
        };
        let touchUp = function (e) {
            e.preventDefault();
            self.element.removeEventListener('touchmove', move, false);
            self.element.removeEventListener('touchend', touchUp, false);
        };
        let mouseDown = function (event) {
            initX = element.element.offsetLeft;
            initY = element.element.offsetTop;
            mousePressX = event.pageX;
            mousePressY = event.pageY;
            down = true;
            window.addEventListener('mousemove', move, false);
            window.addEventListener('mouseup', mouseUp, false);
        };
        let mouseUp = function () {
            if (down) {
                down = false;
                self.element.removeEventListener('mousemove', move, false);
                self.element.removeEventListener('mouseup', mouseUp, false);
            }
        };
        self.element.addEventListener('touchstart', touchDown, false);
        self.element.addEventListener('mousedown', mouseDown, false);

        function move(event) {
            if (down) {
                let left = initX + ((typeof event.touches !== "undefined") ? event.touches[0].pageX : event.pageX) - mousePressX;
                let top = initY + ((typeof event.touches !== "undefined") ? event.touches[0].pageY : event.pageY) - mousePressY;
                if (parent !== null && inside) {
                    if (left >= 0 && left <= (parent.getWidth() - self.getWidth())) {
                        element.element.style.left = left + 'px';
                    }

                    if (top >= 0 && top <= (parent.getHeight() - self.getHeight())) {
                        element.element.style.top = top + 'px';
                    }
                } else {
                    element.element.style.left = left + 'px';
                    element.element.style.top = top + 'px';
                }

            }
        }
    };

    scrollTop(value, speed) {
        let self = this;
        let easeInOutQuad = function (t, b, c, d) {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };
        let start = this.element.scrollTop,
            change = value - start,
            currentTime = 0,
            increment = 20;
        let animateScroll = function () {
            currentTime += increment;
            self.element.scrollTop = easeInOutQuad(currentTime, start, change, speed);
            if (currentTime < speed) {
                setTimeout(animateScroll, increment);
            }
        };
        animateScroll();
    };

    scrollBottom(value, speed) {
        let self = this;
        let startDistance = this.element.scrollTop;
        let endDistance = (this.element.scrollHeight - this.getHeight()) - value;
        let frame = 0;
        let delta = (endDistance - startDistance) / speed / 0.06;
        let handle = setInterval(function () {
            frame++;
            let value = startDistance + delta * frame;
            self.element.scrollTo(0, value);
            if (value >= endDistance) {
                clearInterval(handle);
            }
        }, 1 / 0.06);

    };

    getHeight() {
        return this.element.clientHeight;
    };

    getWidth() {
        return this.element.clientWidth;
    };

    getParentOffsets() {
        let right = 0;
        let bottom = 0;
        if (this.element.parentNode !== null) {
            right = this.element.parentNode.clientWidth - (this.element.offsetLeft + this.getWidth());
            bottom = this.element.parentNode.clientHeight - (this.element.offsetTop + this.getHeight());
        }
        return {top: this.element.offsetTop, left: this.element.offsetLeft, right: right, bottom: bottom}
    }

    getParent() {
        if (this.element.parentNode !== null) {
            this.element.parentNode.id = this.element.parentNode.id === "" ? "snk-copy-" + Math.random() + "-" + Math.random() : this.element.parentNode.id;
            return Snake.el(this.element.parentNode.id);
        }
        return null;
    }

    getOffsets() {
        let offsets = this.element.getBoundingClientRect();
        return {
            top: offsets.top,
            left: offsets.left,
            right: window.innerWidth - (offsets.left + this.getWidth()),
            bottom: window.innerHeight - (offsets.top + this.getHeight())
        };
    };

    setStyle(value) {
        this.element.style.cssText += value;
    };

    getStyle(value) {
        return this.element.style[value];
    }

    hide() {
        this.isHide = true;
        this.setStyle("visibility:hidden;");
    };

    show() {
        this.isHide = false;
        this.setStyle("visibility:visible;");
    };

    onResize(func) {
        let self = this;
        let width = this.getWidth();
        let height = this.getHeight();
        setInterval(function () {
            if (width !== self.getWidth() || height !== self.getHeight()) {
                width = self.getWidth();
                height = self.getHeight();
                func();
            }
        }, 50);
    };

    clear() {
        this.element.innerHTML = "";
    };

    prepend(html) {
        if (typeof html === "object") {
            this.element.appendChild(html.element);
            this.element.insertBefore(html.element, this.element.firstChild);
        } else {
            let appendElem = document.createElement('div');
            let elemId = "snk-append-" + Math.random() + "-" + Math.random();
            appendElem.setAttribute('id', elemId + "a");
            appendElem.innerHTML = html;

            appendElem.firstChild.id = elemId;
            this.element.appendChild(appendElem);
            this.element.insertBefore(appendElem, this.element.firstChild);
            let snkElem = Snake.el(elemId);
            snkElem.parentAppend = elemId + "a";
            return snkElem;
        }


    };

    append(html) {
        if (typeof html === "object") {
            this.element.appendChild(html.element);
            this.element.insertBefore(html.element, this.element.lastChild);
        } else {
            let appendElem = document.createElement('div');
            let elemId = "snk-append-" + Math.random() + "-" + Math.random();
            appendElem.setAttribute('id', elemId + "a");
            appendElem.innerHTML = html;

            appendElem.firstChild.id = elemId;
            this.element.appendChild(appendElem);
            this.element.insertBefore(appendElem, this.element.lastChild);
            let snkElem = Snake.el(elemId);
            snkElem.parentAppend = elemId + "a";
            return snkElem;
        }
    };

    save() {
        let styles = window.getComputedStyle(this.element);
        if (styles.cssText !== '') {
            this.savedStyle = styles.cssText;
        } else {
            this.savedStyle = Object.values(styles).reduce(
                (css, propertyName) =>
                    `${css}${propertyName}:${styles.getPropertyValue(
                        propertyName
                    )};`
            )
        }
        this.savedHtml = this.element.innerHTML;
    };

    restore() {
        this.element.removeAttribute("style");
        this.element.cssText = this.savedStyle;
        this.element.innerHTML = this.savedHtml;
    };

    copy() {
        let elem = this.element.cloneNode(true);
        elem.setAttribute('id', "snk-copy-" + Math.random() + "-" + Math.random());
        let snkElem = new Element(elem);

        let styles = window.getComputedStyle(this.element);
        if (styles.cssText !== '') {
            elem.style.cssText = styles.cssText;
        } else {
            elem.style.cssText = Object.values(styles).reduce(
                (css, propertyName) =>
                    `${css}${propertyName}:${styles.getPropertyValue(
                        propertyName
                    )};`
            )
        }
        return snkElem;
    };

    remove() {
        if (typeof this.parentAppend !== "undefined") {
            this.element = document.getElementById(this.parentAppend);
        }
        if (this.element.parentNode !== null) {
            this.element.parentNode.removeChild(this.element);
        }
        this.clearListeners();
    };

    getHtml() {
        return this.element.innerHTML;
    };

    getText() {
        return this.element.innerText;
    };

    setHtml(html) {
        this.element.innerHTML = html;
    };

    getParents() {
        let nodes = [];
        let element = this.element;
        nodes.push(element);
        while (element.parentNode) {
            nodes.unshift(element.parentNode);
            element = element.parentNode;
        }
        return nodes;
    }

    onclick(func) {
        let self = this;
        let globalFunc = function (e) {
            let parentsId = [];
            let a = e.target;
            while (a) {
                a = a.parentNode;
                if (a != null) {
                    if (a.id !== "") {
                        parentsId.push(a.id)
                    }
                }
            }
            let id = e.target.id;
            if (id === self.element.id || parentsId.includes(self.element.id)) {
                if (typeof self.isHide === "undefined" || self.isHide === false) {
                    func(e);
                }
            }
        };
        this.addListener('click', globalFunc);
    };

    clearListeners() {
        for (let i = 0; i < this.listeners.length; i++) {
            this.element.removeEventListener(this.listeners[i][0], this.listeners[i][1]);
        }
    };

    addListener(name, func, options) {
        this.listeners.push([name, func]);
        this.element.addEventListener(name, func, (typeof options !== "undefined" ? options : true));
    }

    hasClass(cls) {
        return !!this.element.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
    };

    addClass(cls) {
        if (!this.hasClass(cls)) this.element.className += " " + cls;
    };

    addAttribute(name, content) {
        this.element.setAttribute(name, content);
    };

    removeAttribute(name) {
        this.element.removeAttribute(name);
    };

    removeClass(cls) {
        if (this.hasClass(cls)) {
            let reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            this.element.className = this.element.className.replace(reg, ' ');
        }
    };

    delay(callback, ms) {
        let timer = 0;
        let self = this;
        return function () {
            let context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                if (!self.specialKey) {
                    callback.apply(context, args);
                }
            }, ms || 0);
        };
    };
}