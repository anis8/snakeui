import {Element} from "../../Element.js";
import {TextBox} from "./TextBox.js";


export class Editable extends Element {
    constructor(element) {
        super(element);
        this.element = element;
        this.caretPosition = 0;
        this.editable = new TextBox(this.append('<div contenteditable="true" spellcheck="false" style="position:relative;width:100%;white-space: pre-wrap;"></div>').element);


    };

    setPlaceholder(placeholder) {
        let styleSheet = document.createElement("style");
        styleSheet.innerText = `
        [contenteditable=true]:empty:before {
            content: attr(placeholder);
            display: block;
        }`;
        document.head.appendChild(styleSheet);
        this.editable.addAttribute("placeholder", placeholder);
    };

    selectionStyle(command, value) {
        if (typeof window.getSelection() != "undefined" && window.getSelection().anchorNode != null && window.getSelection().anchorNode.parentNode != null) {
            let nodes = [];
            let element = window.getSelection().anchorNode;
            while (element.parentNode) {
                nodes.unshift(element.parentNode.tagName);
                element = element.parentNode;
            }
            if (command === "h1" && nodes.includes("H1") || command === "h2" && nodes.includes("H2")) {
                return document.execCommand('formatBlock', false, 'p');
            }
        }

        if (command === 'h1' || command === 'h2' || command === 'p') {
            document.execCommand('formatBlock', false, command);
        } else if (command === 'foreColor' || command === 'backcolor') {
            document.execCommand(command, false, value);
        } else if (command === 'createlink' || command === 'insertimage') {
            let url = prompt('Veuillez entrer le lien: ', 'https:\/\/');
            document.execCommand(command, false, url);
        } else if (command === "hiliteColor" || command === "backColor") {
            if (!document.execCommand("hiliteColor", false, value)) {
                document.execCommand("backColor", false, value)
            }
        } else {
            document.execCommand(command, false, null);
        }
    };

    editor() {
        let self = this;
        let types = {
            bold: '<b>B</b>',
            italic: '<i>I</i>',
            underline: '<u>U</u>',
            strikeThrough: '<span style="text-decoration: line-through;">A</span>',
            h1: '<b>H1</b>',
            h2: '<b>H2</b>',

            colors: {
                red: 'red',
                blue: 'blue'
            },
            /*
            backColor: {
                red: 'yellow',
                blue: 'green'
            }

             */
        };
        let ul = this.prepend("<ul></ul>");
        for (let type in types) {
            let value = types[type];
            if (type === "colors") {
                let colorsContainer = ul.append('<ul></ul>');
                for (let color in value) {
                    if (value.hasOwnProperty(color)) {
                        let colorValue = value[color];
                        let colorBox = colorsContainer.append('<button>' + colorValue + '</button>');
                        colorBox.onclick(function () {
                            self.selectionStyle("foreColor", colorValue);
                        });
                    }
                }
            } else if (type === "backColor") {
                let colorsContainer = ul.append('<ul></ul>');
                for (let color in value) {
                    if (value.hasOwnProperty(color)) {
                        let colorValue = value[color];
                        let colorBox = colorsContainer.append('<button>' + colorValue + '</button>');
                        colorBox.onclick(function () {
                            self.selectionStyle("backColor", colorValue);
                        });
                    }
                }
            } else {
                let li = ul.append('<button>' + value + '</button>');
                li.onclick(function () {
                    self.selectionStyle(type, null);
                });
            }

        }
    };


    onKeyEvent(func, delay) {
        this.editable.onKeyEvent(func, delay);
    };

    onEnterKeyEvent(func, delay) {
        this.editable.onEnterKeyEvent(func, delay);
    };

    onPasteEvent(func, delay) {
        this.editable.onPasteEvent(func, delay);
    };

    urlify() {
        this.editable.urlify();
    }

    getEditable(){
        return this.editable;
    }
}
