import {Element} from "../../Element.js";

export class TextBox extends Element {
    constructor(element) {
        super(element);
        this.element = element;
        this.caretPosition = 0;
    };

    setHtml(html) {
        this.getCursorPosition();
        this.element.innerHTML = html;
        this.setCursorPosition(this.element, this.caretPosition);
    };

    setText(text) {
        this.getCurrentCursorPosition();
        this.element.innerText = text;
        this.setCaretPosition(this.element, this.caretPosition);
    };

    handleKeys() {
        let self = this;
        this.keysHandled = {};
        let keyup = function (e) {
            self.keysHandled[e.code] = false;
        };
        this.addListener('keyup', keyup);
        let keydown = function (e) {
            self.keysHandled[e.code] = true;
        };
        this.addListener('keydown', keydown);
    };

    onKeyEvent(func, delay) {
        this.addListener('keydown', this.delay(func, (typeof delay === "undefined" ? 50 : delay)));
    };

    onEnterKeyEvent(func, delay) {
        func = this.delay(func, (typeof delay === "undefined" ? 50 : delay));
        let globalFunc = function (e) {
            if (e.code === "Enter") {
                func();
                e.preventDefault();
            }
        };
        this.addListener('keypress', globalFunc);
    };

    onPasteEvent(func, delay) {
        this.addListener('paste', this.delay(func, (typeof delay === "undefined" ? 50 : delay)));
    };

    setCursorAtEnd() {
        let range, selection;
        if (document.createRange) {
            range = document.createRange();
            range.selectNodeContents(this.element);
            range.collapse(false);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        }
    };

    setCursorPosition(el, pos) {
        for (let node of el.childNodes) {
            if (node.nodeType === 3) {
                if (node.length >= pos) {
                    if (pos !== -1) {
                        let range = document.createRange(),
                            sel = window.getSelection();
                        range.setStart(node, pos);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                    }
                    return -1;
                } else {
                    pos -= node.length;
                }
            } else {
                pos = this.setCursorPosition(node, pos);
                if (pos === -1) {
                    return -1;
                }
            }
        }
        return pos;
    };

    getCursorPosition() {
        let element = this.element;
        let isChildOf = function (node, parentId) {
            while (node !== null) {
                if (node.id === parentId) {
                    return true;
                }
                node = node.parentNode;
            }
            return false;
        };
        let selection = window.getSelection(),
            charCount = -1,
            node;

        if (selection.focusNode) {
            if (isChildOf(selection.focusNode, element.id)) {
                node = selection.focusNode;
                charCount = selection.focusOffset;

                while (node) {
                    if (node.id === element.id) {
                        break;
                    }

                    if (node.previousSibling) {
                        node = node.previousSibling;
                        charCount += node.textContent.length;
                    } else {
                        node = node.parentNode;
                        if (node === null) {
                            break
                        }
                    }
                }
            }
        }
        this.caretPosition = charCount;
        return charCount;
    };

    urlify() {
        this.handleKeys();
        let self = this;
        let urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
        let transform = function (text) {
            return text.replace(urlRegex, function (url, b, c) {
                let url2 = (c === 'www.') ? 'http://' + url : url;
                return '<a href="' + url2 + '" target="_blank">' + url + '</a>';
            });
        };
        self.onKeyEvent(function () {
            if (typeof self.keysHandled['ControlLeft'] === "undefined" || (typeof self.keysHandled['ControlLeft'] !== "undefined" && self.keysHandled['ControlLeft'] === false)) {
                self.setHtml(transform(self.getText()));
            }
        }, 10);
    };

}