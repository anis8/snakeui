<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type='module' src='test.js'></script>
</head>
<body>
<style>
    body {
        margin: 0;
    }

    #elem {
        position: absolute;
        height: 100px;
        width: 100px;
        background: red;
    }

    #elem2 {
        position: absolute;
        height: 300px;
        width: 300px;
        background: blue;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #elem3 {
        position: absolute;
        height: 100px;
        width: 300px;
        background: yellow;
        left: 100px;
        top: 100px;
    }

    #elem4 {
        position: absolute;
        height: 100px;
        width: 300px;
        background: yellow;
        left: 100px;
        top: 250px;
    }
</style>
<div id="elem3"></div>
<textarea placeholder="Envoyer un message..." id="elem4"></textarea>

   <div id="elem2">
       <div id="elem"></div>
   </div>


</body>
</html>